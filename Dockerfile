FROM node:18 AS builder

RUN mkdir -p /home/node/fusion-development

COPY . /home/node/fusion-development

WORKDIR /home/node/fusion-development

RUN npm install && RUN npm run build

FROM nginx:latest

COPY --from=builder /home/node/fusion-development/dist/fusion/browser /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

 